# Python code to select which plot to make



from rpy2.robjects import r
import ipywidgets as widgets

def figure_select(nrrows):
    nrrows = nrrows[0]
    b = widgets.RadioButtons(
        options=['all variables', 'per class', 'individual'],
        description='choice:',
        disabled=False
    )

    c = widgets.IntSlider(min=1,max=nrrows,value=1,disabled=False)

    def f(cmap='',individual=''):
        if cmap=='all variables':
            r('ltyPerInd <- unlist(lapply(1:5, rep, times = 10))')
            r('colPerInd <- rainbow(n=10)')
        if cmap=='per class':
            r('ltyPerInd <- 1')
            r('colPerInd <- (Classes+1)/2+1')
        if cmap=='individual':
            r('Indiv <- c({0})'.format(individual))
            r('ltyPerInd <- 1')
            r('colPerInd <- rep(1,50)')
            r('colPerInd[Indiv] <- 2')
    
    out = widgets.interactive(f,cmap=b,individual=c)
    display(out)
