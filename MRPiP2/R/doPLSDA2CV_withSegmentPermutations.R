doPLSDA2CV_withSegmentPermutations <-
function(y, X, nrOfTestSets = 10, nrOfSegmentPermutations = 10, maxLv, scale = "auto", permOrder = NULL) {
  listAverage <- function(lists) {
    listElements <- names(lists[[1]])
    elementsToAverage <- c()
    for (i in 1:length(listElements)) {
      if (is.numeric(lists[[1]][[listElements[i]]])) {
        elementsToAverage <- c(elementsToAverage,listElements[i])
      }
    }
    for (l in lists) {
      
    }
  }
  for (i in 1:nrOfSegmentPermutations) {
   testSetPerm <- segmentMaker(y, nrOfTestSets)
   result2CV <- doPLSDA2CV(y, X, testSets = testSetPerm, nrOfTestSets, maxLv, scale, permOrder, onlyReturnModelResults = TRUE)
   
  }
}

