permutateAndShowCoefficentsPlot2CV <-
function(resultsOf2CV, noPermutations = 10, significance = 0.95) {
  getCoefficientMeans <- function(plsda2CV) {
    nonpermutatedCoefficientMeans <- array(0,dim=dim(plsda2CV$plsdaModels[[1]]$coefficients[,,plsda2CV$plsdaModels[[1]]$ncomp,drop=FALSE]))
    for (i in 1:length(plsda2CV$plsdaModels)) {
      nonpermutatedCoefficientMeans <- nonpermutatedCoefficientMeans + plsda2CV$plsdaModels[[i]]$coefficients[,,plsda2CV$plsdaModels[[i]]$ncomp,drop=FALSE]
    }
    nonpermutatedCoefficientMeans <- nonpermutatedCoefficientMeans/length(plsda2CV$plsdaModels)
    nonpermutatedCoefficientMeans[,,1]
  }

  foundCoefficientMeans <- getCoefficientMeans(resultsOf2CV)
  permCoefficientMeans <- array(NA, dim=c(noPermutations,length(foundCoefficientMeans)))
  
  pb <- txtProgressBar(min = 0, max = noPermutations, style = 3)
  
  for (p in 1:noPermutations) {
   permRowOrder <- sample(1:dim(resultsOf2CV$y)[1])
   permResultsOf2CV <- doPLSDA2CV(y = resultsOf2CV$y, X = resultsOf2CV$X, testSets = resultsOf2CV$testSets, maxLv = resultsOf2CV$maxLv, scale = resultsOf2CV$scale, permOrder = permRowOrder, onlyReturnModelResults = TRUE)
   permCoefficientMeans[p,] <- getCoefficientMeans(permResultsOf2CV)
   setTxtProgressBar(pb, p)
  }
  close(pb)
  
  upperBoundConfidenceInterval <- array(NA, dim=c(1,length(foundCoefficientMeans)))
  lowerBoundConfidenceInterval <- array(NA, dim=c(1,length(foundCoefficientMeans)))
  oneSidedSignificance <- 1-((1-significance)/2)
  for (c in 1:dim(permCoefficientMeans)[2]) {
    mean <- mean(permCoefficientMeans[,c])
    sd <- sd(permCoefficientMeans[,c])
    error <- qnorm(oneSidedSignificance)*sd
    upperBoundConfidenceInterval[c] <- mean + error
    lowerBoundConfidenceInterval[c] <- mean - error
  }
  
  list(foundCoefficientMeans,upperBoundConfidenceInterval,lowerBoundConfidenceInterval,permCoefficientMeans)
}

