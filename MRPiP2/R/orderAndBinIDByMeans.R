orderAndBinIDByMeans <-
function(IDorderedM, valueCol, IDsPerBin, quiet = FALSE, repeatsPerID = -1) {
  # first, find out in which order the IDs (rows containing repeats of the same sample-measurement) should be binned together
  IDOrderByMean <- sort(IDorderedM$meansPerID[ ,valueCol], index.return = TRUE)$ix
  
  result <- list()
  binCounter <- 0
  currentBin <- 0
  samplesDiscarded <- 0
  for (id in IDOrderByMean) { # go through each ID in order of increasing mean-value
    if (binCounter == 0 || binCounter > IDsPerBin) { # either first bin or previous bin is full: start a new one
      currentBin <- currentBin + 1
      result[[currentBin]] <- list()
      binCounter <- 1
    }
    if (length(IDorderedM$IDs$indicesPerPattern[[id]])>1) { # only include IDs in a bin when there are at least two repeats
      if (repeatsPerID != -1 && length(IDorderedM$IDs$indicesPerPattern[[id]])>repeatsPerID) { # only include a max amount of repeats per ID (or multiple IDs, if enough repeats are available)
        ranges <- seq(0,length(IDorderedM$IDs$indicesPerPattern[[id]]),repeatsPerID)
        for (r in 2:length(ranges)) {
          rangeStart <- ranges[r-1]+1
          rangeEnd <- ranges[r]
          result[[currentBin]][[binCounter]] <- IDorderedM$M[IDorderedM$IDs$indicesPerPattern[[id]][rangeStart:rangeEnd] ,valueCol, drop = FALSE]
          binCounter <- binCounter + 1          
          if (binCounter > IDsPerBin) { # either first bin or previous bin is full: start a new one
            currentBin <- currentBin + 1
            result[[currentBin]] <- list()
            binCounter <- 1
          }     
        }
      }
      else { # include all repeats in single ID
        result[[currentBin]][[binCounter]] <- IDorderedM$M[IDorderedM$IDs$indicesPerPattern[[id]] ,valueCol, drop = FALSE]
        binCounter <- binCounter + 1
      }
    }
    else {
      samplesDiscarded <- samplesDiscarded + 1
    }
  }
  if (length(result[[currentBin]])==0) { # check whether the last bin created actually received any valid entries (otherwise: delete)
    result[[currentBin]] <- NULL
    currentBin <- currentBin - 1
  }
  if (currentBin<2) {print("Too few repeating sample were found to create 2 bins or more. Please adjust bin-size or select a column with more sample values."); return(NULL)}
  if (!quiet && samplesDiscarded > 0) {
    print(paste("During binning,",samplesDiscarded,"samples had no further repeats, and were therefore discarded."))
  }
  # per bin, calculate mean, and calculate the mean variance over all IDs in that bin
  result$meanPerBin <- array(0,dim=c(currentBin))
  result$errorPerBin <- array(0,dim=c(currentBin))
  for (bin in 1:currentBin) {
    variancesPerID <- c()
    for (samplesPerID in result[[bin]]) {
      meanThisID <- mean(samplesPerID)
      variancesPerID <- c(variancesPerID,
          sum( (samplesPerID-meanThisID)^2 ) / (length(samplesPerID)-1)  
      )
    }
    result$meanPerBin[bin] <- mean(unlist(result[[bin]]))
    result$errorPerBin[bin] <- mean(variancesPerID)
  }
  
  result
}

